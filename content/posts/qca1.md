---
title: First Foray into Big Projects
date: 2018-06-25
draft: false
category: article
description: An excerpt about Quake Champions Academy
---

# Quake Champions Academy Back from the Dead

Finally after nearly a year I've started working on my Quake Chamions Academy project again but this time I have some specific goals in mind besides just, run lots of community events. 

## Some Background First

Quake Champions Academy is a discord server/community  that I set up about a year ago when QC(Quake Champions) went into the open beta with the initial inention of using the server to host 2v2 tournaments every weekend. 
Of course this was a massive time sink and couldn't really do it every weekend, so eventually had to stop.
In the meantime I managed to acrue around 100 or so members to the channel so I figured I would need more reasonable method of keeping people around.
This is where I came up with the idea of creating an "Academy" Server where people can come learn about the game and maybe meet some new people in the process.


Ultimately the server died down quite a bit with the game as well but since it went free-to-play for 2 weeks it means that tons of new playesr have been coming in.
Since there were so many new players, and people seemed to be actually actually enjoying *playing* the game I saw it as a perfect time to get the old website idea into reality. 
There was one main problem... the people who can help me is not plentiful and don't know how to use tools like git/HTML/CSS/JS etc etc.
So I eventually settled, (*very unwillingly*) on hosting the site on a google site. 

## Hoo boy into the lion's den

I'm aware of how closed down of a platform sites.google.com really is but honestly I'm also aware of the danger of keeping barrier to enty for contribution unrealistically high for other people. 
I won't be able to get to open sourcing the site just yet but at some point I will be getting to that, right now it's a matter of turning into a kind of wiki for new players to use as a guide so that they aren't so lost when playing.

At least for now the site is up, and it's getting useful information uploaded to it, philosophy aside it provides a practical use which I beleive to be of higher precedence at this time than making it free. 

For now if you want to check out the site you can look for it here: \
https://sites.google.com/view/qcacademy/home


# Syncthing and Remote File Management

> What is [syncthing](https://syncthing.net/)?

It's a free and open source project that basically keeps your files synchronized across multiple devices.

It woks like so(in my setup):

```
Listen Server => Gets folders and things shared with it
PC => usually uploads stuff to the listen server
Phone => usually just pulls down updates from the listen server
```

I personally use Syncthing to keep my ever growing music library and Book/Papers collection up to date on my phone and PC.

> Can you cut out the middle man by removing that server?
Yes but I'm lazy and this give me the option of running syncthing on my laptop and pull down changes from anywhere without much hassle.

> Where do yo host this listen server?
For hosting I use [IONOS](https://www.ionos.com/) since VPS's only cost 2 USD a month. Currently it's running Debian 10 Buster.


---
title: Making Cola 
date: 2021-12-30
draft: false
description: From Pure Homemade ingredients
category: article
image: /media/thumbnails/cola.jpg
---

{{< pic "/media/img/cola1/finish.jpg" >}}

# Taste

I'm ignoring carbonation since everyone is going to probably use something
different. _However_, I used a Soda Stream set to **2** which gave me a
very fine carbonation and a very nice head when poured over ice.

Taste wise this is going to be very flowery, cinnamon-y, cola.

# Ingredients

_Makes 4 cups of syrup total_.

| Amount | Item |
|------|----|
|1 tsp   | Cinnamon |
|1 tsp   | Nutmeg |
|1 tsp   | Coriander |
|1 tbsp  | Brown Sugar |
|1/2 tsp | Anise |
|1/2 tsp | Citric Acid |
| 2      | Oranges worth of **zest** |
| 1      | Lemon worth of **zest** |
| 2 Cups | Water |
| 2 Cups | Sugar |


# Process


Mix the following together:

* Cinnamon
* Nutmeg
* Coriander
* Brown Sugar
* Anise
* Citric Acid
* Oranges zest
* Lemon zest
* 2 cups of water

{{< pic "/media/img/cola1/dry-bowl.jpg" >}}

{{< pic "/media/img/cola1/sugar.jpg" >}}

{{< pic "/media/img/cola1/zest.jpg" >}}

Mix this up in a pot and get it to simmer for 20 minutes.

{{< pic "/media/img/cola1/pot.jpg" >}}

While the pot is simmering take the 2 cups of sugar and blitz it in a food
processor. Doing this makes the sugar dissolve more easily later on when we
mix everything.

For now while waiting move the sugar to a large bowl.

Once the 20 minutes on the pot is up grab a fine sieve and strain the pot's
mixture through into the bowl with the sugar. Let cool and mix occasionally for
about 15 minutes.

You're looking for a simple-syrup consistency.

Once cool you can mix with club soda, sparkling mineral water, or if you have a
Soda Stream use that, to add some carbonation.

## Ration for soda

**4:1** of Water to syrup.


# Additional Notes

You can also add 1 tsp of vanilla bean, or like a splash of vanilla extract
near the end when you are mixing with the sugar.

If the final product is too syrupy then turn it down to like a **5:1** ratio.
Any lower and you might start to dilute the flavor too much.
 

Finally if you don't like the color then you can add blue + red to make the color
a bit darker, similar to the pic below. I used **5 blue to 4 red**.

{{< pic "/media/img/cola1/fake-bottled.jpg" >}}

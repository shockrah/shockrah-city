---
title: "Delivering whole OS's in Gitlab's CI/CD"
date: 0000-00-00
draft: true
---

# Delivering whole OS's in Gitlab's CI/CD
## Way easier than you may think

Lately I've been trying to learn about deploying things in remote pipelines continuously so I've taken to using Gitlab's own provided CI pipelines to... well _do stuff_.
After some bumps in the road I've learned a few things: 

## Setting up docker images isn't that hard

Maybe it's because I'm not doing _complex things_ or maybe because I've dealt with some awful problems on Debian before but setting up Debian images really aren't hard to setup.
The best part is that unlike real full installations they're super easy to just nuke and start fresh over again(assuming you get to that point).
Usually however I just tweak the Docker script, re-run and done.

> Where's the image bro

Here you go: [dank docker image](https://hub.docker.com/repository/docker/shockrah/osdev-build)

To save you a click, its a docker image which I use to provide ISO's of my [handwritten operating system](https://gitlab.com/shockrah/jankos).
It's nice because now I can easily provide bootable files for people to test the code I've written without having to compile any code!

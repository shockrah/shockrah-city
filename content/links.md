---
title: Places you can find me
description: Click on them and find out 
type: contact
layout: stream
links:
    - target:
        name: Freechat
        url: "https://freechat.shockrah.xyz/"
        image: "https://freechat.shockrah.xyz/bear.gif"
        description: "FLOSS chatting for everyone, come chat fren =|:^)"
        title: "The Freechat Reference Famalam"
        color: "#c81d8d"
    - target:
        name: Gitlab
        url: https://gitlab.com/shockrah/
        image: https://gitlab.com/uploads/-/system/user/avatar/868053/avatar.png
        description: =|:^) Like my hat? =|:^)
        title: shockrah
        color: "#FCA326"
    - target:
        name: Portfolio
        url: "https://resume.shockrah.xyz"
        image: "https://shockrah.gitlab.io/resume/images/banner.png"
        description: "Open source Software Developer"
        title: "Alejandro Santillana"
        color: "#ffc107"
    - target:
        name: Twitch
        url: https://twitch.tv/shockrah
        image: https://static-cdn.jtvnw.net/ttv-static-metadata/twitch_logo3.jpg
        description: Twitch streams every blue moon
        title: shockrah
        color: "#9146FF"
    - target:
        name: Steam
        url: https://steamcommunity.com/id/shockrah
        image: https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/30/30d8ebff1171308593ad9fe8d7631a879de8c315_full.jpg
        description: "jumping is cool (✿◠‿◠)"
        title: "Steam Community :: shockrah (✿◠‿◠)"
        color: "#66c0f4"
    - target:
        name: "Main Site"
        url: "https://shockrah.xyz"
        image: https://shockrah.xyz/favicon.png
        description: "Place for me to put stuff I think is shareworthy"
        title: "Shockrah's Blog"

    - target:
        name: "Quake Academy"
        url: "https://sites.google.com/view/qcacademy/home"
        image: https://lh3.googleusercontent.com/IYCmS_9X4hOy7gh3p2WSu7Pc9951-Mp_2a07LtzO-INjS8DekyvQgNHab4Slv0BLNprFrg=w16383 
        description: "Learn and Improve. Check out our guides page if you want to improve or just want a quick reference for something."
        title: "(No longer maintained)"
        color: "#b00e0e"
---


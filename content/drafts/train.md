# Trains are Weird

## y does cali succ at train 

I've come to the conclusion that California has no idea how to do public transportation.
Trains for instance get some of the __F A T T E S T__ delays imaginable.
Not unlike any other system of transportation like planes or buses, but with trains there's the added bonus that it can stop, and do nothing, mid travel.

Most countries seemed to have figured out that putting trains on street level means car traffic slows down, and trains have to slow down too.
Here in Cali though, we don't use train tunnels in cities.
_Before you say something about earthquakes, that's not actually the reason why_.
The actual reason, we just suck at public transport.
The absolute state of the freeways here is also pretty hilarious; instead of building better freeway routes we just add more lanes which compounds traffic problems.

## still neat tho

Not even gonna lie, it's kinda fun being on the train; _when you're actually moving_.
Mostly because people that go on trains lose their minds about being locked in a rolly tin can in the woods really quick.
That means after about an hour and in people start wandering around and dropping the maddest _truth bombs_ and _conspiracy theories_ I've ever seen.

Sick shit to witness and journal about but after delays, __literally every trip__ I've decided that I'm just going to spend a tiny bit more and take the plane to get around Cali.

_Shoutout to the guy that crashed into the train ahead of me in the middle of rush hour traffic.
That s1ck 2 hour delay was litty._

---
title: "Rewrites and this site"
date: 2019-05-02
draft: false
category: article
description: How I'm rebuilding this website
---

# Rewrites
## For the sake of rewriting things

Finally after so long I came back to make this site look a little bit better since I never had a theme in mind.
I'm still changing up the theming a bit but this time around it should hopefully be a bit more _modern_.

Still no non-free javascript tho `\:D/`.
In fact at the moment there is literally 0 lines of javascript which actually runs on the site right now.

Even tho, [purecss](https://purecss.io/) has some javascript, it's only used for Node module packaging. 
Since I don't have any javascript using Node, those script never execute.

Also the make script has been updated(praise kek).
If you want to build your own websites with [Pandoc](https://pandoc.org/) & [markdown](https://commonmark.org/help/), you can use the newly updated build script below.


This is the most recent _stable_ build of the script but there's a more bleeding edge version [here](https://gitlab.com/shockrahwow/shockrah-city/blob/master/make.sh).

```sh
#!/bin/bash
rootDir='./tmp/'
targetDir='post/'

[[ -z $rootDir/post/ ]] && mkdir -p $rootDir/post

post() {
	# First get the body of the document
	tmp=".swap"
	base=$(basename $1)
	# cat the files together
	pandoc $1 | cat 'templates/post-header.html' - 'templates/post-footer.html' >> $tmp
	sed -i "5i <title>${base%.*}</title>" $tmp
	sed -i "s/<img/<img class=\"pure-img\"/g" $tmp
	sed -i 's/<table>/<table class="pure-table">/g' $tmp
	# turn the header into an actual header
	sed -i '30i <div class="header">' $tmp
	sed -i '32i </div>' $tmp	# change this to 33 once we finalized for the subheading
	# finally move this post to its proper location in the _rootDir
	mv $tmp "$rootDir/$targetDir/${base%.*}.html"
	rm -f $tmp
}

for file in $@;do
	post $file
done

if [ -z $@ ] 
then
	while read line
	do
		post $line
	done < "${1:-/dev/stdin}"
fi
```

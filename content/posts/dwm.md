---
title: "DWM > i3"
date: 2019-08-18
draft: false
category: article
description: I'm finally settling on DWM
---

# A month in DWM - Moving forward with my setup

For about a year and half I've been using [i3wm](https://i3wm.org/) as my daily driver under Debian and I've loved it thus far. 
But after a while it started to get old and I wanted something new without having to distro-hop.

[DWM](https://dwm.suckless.org/) interested me as its memory usage footprint is tiny compared to i3wm and follows the [suckless philosophy](https://suckless.org/philosophy/) which I found interesting and appreciate more and more.

## Is it /comfy/?

Absolutely. I'll let this little clip speak for itself:

<div style="width: 100%; height: 0px; position: relative; padding-bottom: 28.125%;"><iframe src="https://streamable.com/s/skb0y/tbxohj" frameborder="0" width="100%" height="100%" allowfullscreen style="width: 100%; height: 100%; position: absolute;"></iframe></div>

Once you get past a couple of hurdles with it things becomes really nice and to be honest have spoiled me to the point where basic tasks in other setups has become painful. 
_If you're a vim user then you'll know the feeling._

## Hurdles

Configuring anything blows. Recompiling things is just dumb and means you have to:

1. Close everything because you will have to exit DWM + X11
2. Exit DWM +  X11
3. Make changes & recompile
4. Test changes
5. Tweak changes and go back to step 2

The splitting is... well it's awful.
The idea is neat and on paper makes logical sense but to be honest anything past two windows(sometimes three) is no fun to use and honestly not even that useful in anyways.

## The /comfy/

Now that I've messed with things my setup it feels much more personal, though that may be because of the nice backgrounds I now have.
The lack of features has sorta grown on me to where I don't care that it doesn't have a bunch of random features and enough docs to fill a book.
It displays windows in a neat fashion and is friendly with multiple monitors.

## Conclusion

Yea I like it, it's probably going to be my daily driver a while until a port for Wayland comes out since the only thing left to fix is the screen tearing which is no good.

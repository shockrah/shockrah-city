---
title: Notes and Ramblings of mine
description: Usually poorly written but still stuff I think is neat
---
# Notes and Things

This page is dedicated to my own notes about stuff that I read.
It's _very_ stream of concious-y and often not super coherent so often there gaping logical holes but I like the way they _feel_ so I link them on this page.

**NOTE:** A lot of these aren't actually finished since they are ultimately just notes which describe my contemporary thoughts when reviewing something new.

## Philosophy

* [Is Logical Positivism a Meme? - Against Method](/notes/against-method)


## Cookery

* Nothing Yet

## Math Things

* Nothing Yet

## Design things

* Nothing Yet


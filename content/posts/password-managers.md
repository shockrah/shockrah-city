---
title: Password Managers
description: just use unix pass
date: 2024-04-30T16:12:04-07:00
draft: false
category: article
---

# `pass` Shilling
This one is gonna be short but I've come shill password manager that:

* Allows for simple generation of passwords
* Allows for easy backups to be made
* Is compatible with basically any password access workflow you can think of
* Is completely foss
* Allows for both single and multiline passwords
* Allows for optional opt-in versioning using git ( as a subcommand )
  * Can also be used with other VCS's

It's called `pass` --> https://www.passwordstore.org/

# Examples

Here's an example of how it works


Simple Generation of passwords: 

```
~ pass generate example 100
[master 3cfe907] Add generated password for example.
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 example.gpg
The generated password for example is:
9m/Mk5WzLE75=QzhdD;T>}keXOv')FOh~S(=J43OAZ2qkxg<I>hjUJGRpav%oI<yq!ULw0<)@#P|+y0C2~q[O2=&^x{P_\v'K@\B
```

Want to straight up just copy it directly? Ok here you go with `-c`

```
pass generate example2 100 -c
[master 55599f0] Add generated password for example2.
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 example2.gpg
Copied example2 to clipboard. Will clear in 45 seconds.
```

# Why tho

Because it's completely dead simple to the point of absurdity. Want a really
weird use case; pipe the output into a `qr` command and generate completely
random qr codes. Using the same example as above:

```
pass example | qr
█████████████████████████████████████████████████
█████████████████████████████████████████████████
████ ▄▄▄▄▄ ███▄▄▄ ▀▀▀▄▀▄▀ ▄▀▀▄ ▀█▄▀▄ █ ▄▄▄▄▄ ████
████ █   █ ████ █▀▄▀ ▀ ▄█▀ ▄ █▀▀█▄ █▀█ █   █ ████
████ █▄▄▄█ █▀▄  ▀▄▀ ▀█ ▀▀▄█▄▀ ▀▀ ▄▀▀ █ █▄▄▄█ ████
████▄▄▄▄▄▄▄█▄▀▄▀ ▀ ▀ ▀ ▀ ▀▄█▄█ █▄▀▄█▄█▄▄▄▄▄▄▄████
████▄▀█ █ ▄▀  ▀▄▀▄  ▄▄▀▄▀█▀██   ▀▄█ ▀▄▀▄█▀▀█▀████
████ █▄▄ ▀▄  ▄█▀▄▄ █ █▄▄▀▀██  ▄▀ █▀▄ ▄ ▄▄█  ▀████
████▀▄▄▄█▄▄▀██ ▀█ ▄▄▄▄█▄█ ▀▀▀▀▀ ███▄▀█▄██▀█▀ ████
████▄▀ █▀▄▄  ▀▄▀ ▄ ▄▀  ▀▄▀▄ ▄▀▄█▀ ██▀█▀█▀ ▄  ████
████▀▄ ▀█▄▄ ▄▄█  ██▀ ▄ ▄  █▀▄▀█   ▀ ▄█▀▀█▄▀▀ ████
█████ ▀▀▀█▄▀▄████ ▄▄ ▀ █   ▀ ▄█▀▀▀▄▀▀▀▀▄  █▀▄████
████ ▀ ▀██▄▀ ▀▀ ▄█ ▄▀▄ ▄▄▀▄█ ▄█▀ ▀▀▄█▄ ▄▄▄▀▄ ████
█████▀▄██▀▄▄ ▄█▄█▀▀█▄█▄ ▀▀▄██▀ █▄█▀▄▀ ▀▄▀▄▀██████
████▀█ █▄ ▄ ▄   █ ▀ ███   ▄▀  ██▀▄ ▀▀▀▄██ ▀▄▄████
█████ ▄ ▀█▄█  ▄██▄▀ ▀▀▄▀  ▀   █▀▀▀▄█ ▀  ▄ ██ ████
████▄▀▄▄██▄▄▄  ▀▀▄▄▄▄█▀▀▄ ▀█ ▀▄▀   ▄ █▀▀▄ █▀█████
████▄██▀  ▄▄ ▀▄▄██ ▄▄▀▄   ▄██▄▀▀ ▀ █▀▄ ██ ▄▄ ████
████▄███▄▄▄▄ ▀▄ █▀ ██▄█▄ █▀██ ▀█▄█▀  ▄▄▄ ▀█▄ ████
████ ▄▄▄▄▄ █▀▄▄ ▄▄▄▄▄ █   ██▄▀█▀▄▄ █ █▄█ ▄▀▄▀████
████ █   █ █▀ ▀ ▀▀▀▄ ███ █ █▀▀██▀▀▀▀▄▄  ▄▄█ █████
████ █▄▄▄█ ████▄▀█▀ ▄▀▄▀▀ █▄▄▀█▀ ▀▀▄█▄ █ ▄█ ▄████
████▄▄▄▄▄▄▄█▄▄█▄█▄█▄▄▄███▄▄█▄███▄▄▄█▄█▄▄██▄▄█████
█████████████████████████████████████████████████
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
```

Also each password is just a file in a directory tree which means you can sort
things however  you'd like which is also neat. Anyway that is all I had for now
just thought this program I've been using for a while now was cool and deserved
a quick post about it :smile:


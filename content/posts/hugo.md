---
title: Building my Blog with Hugo
description: Why I'm switching away from simple bash scripts
category: article
draft: false
date: 2021-09-10
image: "/media/thumbnails/hugo.png"
---

For a while now I've been using `pandoc` and a combination of shell scripts to build and deploy my site via a gitlab CI/CD pipeline.
Given that I want to start sharing my site to more people I realized I need a way to generate some meta data for each page.
In order to this I have to somehow:

* Tag each page with meta data that other sites can look at
* Make sure the build system is aware of this new meta-data

Inserting meta data into posts and page source code isn't too hard but there is the problem of standardizing a format which meets my criteria of:

* Easy to write
* Easy to parse
* Preferably not verbose to keep things small
* As robust as possible

That last one is the part that makes this really hard to implement if I'm being honest.
Yes I could come up with a format and use something like `sed` or `awk` to replace markdown with inline HTML and do some wizardy on the compilation script to `cat` partial html files together properly but...
After having already done similar things for other projects I know exactly how painful that can be, especially to do it well.



## Why Hugo

1. Instant support for yaml meta data in each markdown file
2. Pages are built out of go-templates with javascript
3. Templates

It's literally just the templates.
The fact is I have two templates that I care about right now: `posts` and `index`.

> What about styling and making sure you don't lose the charm of the site?

I can literally re-use the same style sheet from before with Hugo and fonts can also stay where they are.
No Javascript is ever compiled and sent to the browser.

> What build advantages do you get from this?

Robustness. I can build out tons of pages in reasonable amounts of time with no mysterious errors and have a ton of flexibility when it comes to adapting templates to be something new.

> Aren't you locked into a single theme this way?

Yes but most people's themes send a ton of Javascript which I don't want to do ever

> Can I see what the theme looks like?

Sure: [click here for the theme](https://gitlab.com/shockrah/shockrah-city/-/tree/master/themes/shockrah.xyz)




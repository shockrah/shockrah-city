---
title: "How this site came to be"
date: July 22, 2018
draft: true
---

# How this site came to be

## Tech

The """"tech-stack"""" for this site is [pandoc](https://pandoc.org/) and a single shell script.
Ok, technically there's also the issue about python but really you don't need to use it since the site pages build anyways.

Yea so here's the shell(bash) script.
I'm going to remove any bashisms soon enough but am very lazy so this is the best I got for ya.
```
#!/bin/bash
rootDir='./site/'
targetDir='post/'

post() {
	echo Building: $1
	tmp=".swap"
	full=".fullswap"
	base=`basename $1`
	# First build the content of the site
	pandoc $1 > $tmp
	# Title, images and tables get added/fixed here
	sed "s/{TITLE}/<title>${base%.*}<\/title>/g" ./temp alias in your bashrc and you’re good to go.

Rambled guess time
My guess on how the alias is that bash doesn’t try to figure out that you want to use vim since you’ve only typed a single v in this case; therefore tab-completion would go unnoticed?
Say you had a directory with files:

first
second
lates/post-header.html > $full
	cat $tmp ./templates/post-footer.html >> $full
	sed -i 's/<img/<img class="pure-img"/g;s/<table>/<table class="pure-table">/g' $full

	# Move things to proper directory and cleanup
	mv $full "$rootDir/$targetDir/${base%.*}.html"
	rm -f $tmp
}

build_all() {
	mkdir -p $rootDir/post/ $rootDir/fonts/ $rootDir/media/img/

	cp style.css "$rootDir"
	cp prebuild/post/style.css "$rootDir/post/"

	cp fonts/* $rootDir/fonts/

	cp img/ $rootDir/media/img/ -r
	mv $rootDir/media/img/favicon.png $rootDir/

	# Try to get xargs to deal with this ugliness
	for r in prebuild/*md;do
		post $r
	done
	for p in prebuild/post/*md;do
		post $p
	done
}

_help() {
	echo 'Options: 
	-h show this prompt
	-p [postDirectory/*] (takes a list of paths to build from)
	-r build root pages 
	-s copy stylesheets
	-l run live server'
}
server() {
	cd $rootDir
	echo 'Address: 0.0.0.0:8080'
	python -m SimpleHTTPServer 8080
}

if [ -z $1 ]
then
	_help
	exit 0
fi

while getopts ":aslrhp:" opt;do
	case "$opt" in
		a)
			build_all;;
		s)
			cp style.css "$rootDir"
			cp prebuild/post/style.css "$rootDir/$targetDir"
			exit 0
			;;
		r)
			# Fix targetDir so that it points to the root of the site output
			targetDir=''
			post './prebuild/about.md'
			post './prebuild/links.md'
			post './prebuild/index.md'
			exit 0
			;;
		p)
			for file in ${@:2};do
				post $file
			done
			;;
		h)
			_help;;
		l)
			server;;
	esac
done

```

## Story

# Against Method: study against logical positivism

Word: I should note that this is written while reading through Feyeraband's essay. Henceforth expect knee-jerk-reaction type of writing as opposed to well thought out logical anything.

Anything properly _fleshed out_ is labeled as such.


## Observations & Connections Based on Recent Things Around Me

A video from _Periodic Videos_ comes to mind wherein they tells a story about someone that tried to make a _mini-nuclear reactor_ in their kitchen.
The tone of the speaker in the video is in some ways mocking the guy for trying to do this in their own home and not in a lab.
Some believe that he [reactor-guy] was trying to split atoms and so he used some elements in a mix but it went wrong and blew up his stove-top.

> _What if he made a great discovery?_

When asked about this the speaker [speaker-guy] mentions that reactor-bro shouldn't be doing this in his home because its not safe and could kill him (reminder that battery engineers complain the same about home power cell diy'ers [see: muh safety argument]).

There's also random clips of nuclear testing demonstrations from the 50's which are seemingly randomly thrown in as if to insinuate that reactor-bro was trying to make or bomb or could have _nuked'd_ himeself.
It seems a bit out of place and weird.

PDF2LEFT: states about the epistemological doctrine of classical scientists in history: "who works in a particular historical situation must learn how to recognize error and how to live with it". Going on to say that they need a "_theory of error_ in addition to the _certain and infallible_ rules which define the approach to the truth.
Further: To develop a theory of error is to create an (likely unchanging) _theory of error_ is to riddle that same theory with historically sourced error. 
In other words the theory itself is not free from the very thing that it describes as a problem to scientific development. [see observational bias, infinitesimal regression or observation]

## Something about a form of indoctrination I thought was cool

> Almost everyone now agrees that what looks like a result of reason - the mastery of a language, the existence of a richly articulated
perceptual world, logical ability - is due partly to indoctrination, partly a to a process of growth that proceeds with the force of natural low

Not much to say other than I thought it was interesting how indoctrination is  used here in an almost innocent fashion.
Indoctrination is used in conjunction with the phrase _due partly_ which seems as though the feeling is indoctrination is there but with its weight removed.
Just before this part Feyerabend talk about error related to historical periods which seems rather fitting as imperceivable indoctrination could easily be part of this _historical error source_.


## Breaking the rules of classical epistemology

> We find, then, that there is not a single rule, however plausible, and however firmly grounded in epistemology, that is not violated at some time or other.

Hmm

> Such violations are no accidental

Hmmmmmm

> they are not the results of insufficient knowledge or of inattention which might have been avoided

Hmmmmmmmmm!

> On the contrary, we see that they are necessary for progress

Progress in this case really means "knowledge".
I'm choosing to interpret this using Bloom's method of describing understanding. New discoveries don't always give us comprehensions as we've really only uncovered an apparant fact.
Comprehension, understanding, and intuition come later after some time.

So we see that in order to progress scientific knowledge that it is necessary to break from traditional methods and embrace that which traditional processes often deem incorrect.

Memes aside this reads similar to the colloquialism that goes something like: _use the right tool for the job_.
Following this casual logic we could prescribe traditional scientific method(a process) analogous to some kind of tool, which implies there are specific uses for it.

## Counterinduction 1: Theories

Cool points that are made in this section:

> Progressive educators have always tried to develop the individuality of their pupils, and to bring to fruition the particular and sometimes quite unique talents and beliefs that each child possesses

To this end I see that creativity in the pursuit of solutions flourishes most when students are given constraints.
One of the fundamental issues with finding solutions to problems is finding the appropriate question to ask which allows the creative part of ourselves to make connections between the problem and the question being asked.
Those connections and the process of establishing premises around the convenience, feasibility, and effectiveness of those connections is what facilitates the derivation of a solution.

Simply said: if you can ask a good enough question then creativity can flourish in the space of that question. Then it is sufficient to understand how we ask questions.

## Asking questions is both Restrictive and Freeing

### It give us a reason to move

I say this because through out the course of this essay there is a sentiment which I personally don't agree with _(bad paraphrasing incoming)_:

> A process of learning free of restrictions is also free of restriction of thought

On paper yes, this is probably how things should work, however: as irrational a person may be we are still wrapped in this logical, rational body of a human.
This is to say that while we may be able to conjure great theories without restrictions in practice many freeze at the thought of having no restrictions to a solution.

A question regarding a problem gives us context; a problem space.
Even if we are trying to derive some new kind of knowledge from what may seem like the ether before we arrive at an uncharted logical destination we must at least generate some set of problems spaces, in order to align a direction of solution.


## Consensus

"In addition, pluralism is supposed to lead to the truth: '...the peculiar evil of silencing the expression of an opinion is that it is robbing the human

#!/bin/sh
set -e

# Build to optimize images and not much else if im being honest
usage() {
cat <<EOF
Summary:
	Will compress and resize images so that they don't take up too much space
	and are easier on traffic usage
Usage:
	optimize.sh [files to optimize...]
EOF
}

if [ -z "$1" ]; then
	usage
	exit 1
fi

mkdir -p output/
for f in "$@"; do
	convert -resize 900x $f output/`basename $f`
	convert -strip -quality 85 $f output/`basename $f`
done

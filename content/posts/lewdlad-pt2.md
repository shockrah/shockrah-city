---
title: Lewdlad - Project Bucket
date: 2022-09-13
draft: true
description: A newer approach to multiplexing game servers 
category: article
image:
---

In a [previous post](https://shockrah.xyz/posts/lewd-lad-infra/) I had laid out
the infrastructure that was built around the idea of having EC2 instances lay
dormant when not in use and toggle them on/off with the use of a Discord 
command. For a while now this has worked great:

* Bot is containerized and can easily be deployed
* Infrastructure is managed by Terraform
* Server maintenance is tracked by changes to Ansible playbooks
* Server IP's are easily exposed with Dynamic DNS
	* This is important so that users don't have to mess with configs as most games save the last IP/Hostname was for a given server (i.e. minecraft)

# Motivation for a new infrastructure 

While this setup has worked really well so far the cost has been a lot higher 
than it could be.



---
title: "Using Gitlab CI For VPS and Neocites Deployment"
date: 2021-02-08
draft: false
category: article
description: Removing pain points from updating the Neocities page
---

# Using Gitlab CI For VPS and Neocites Deployment

_I'll make this one short_: Deploying to a VPS is pretty easy when its already configured with Nginx to serve static content, but Neocities is a slight worse pain. Several scripts later I've figured out how to 100% automate pushing to both.


## Docker

A few notes about this docker file:

* Could be made way smaller if using Alpine

* Setting up Pandoc on alpine is way more effort on Alpine because you have to compile it yourself as its not in the official Alpine repos

* gensite is a script I wrote for building markdown based sites with Pandoc with 0 deps

```
FROM debian:sid-slim

RUN apt-get update && apt-get install -y git pandoc
# For more easily logging into ionos
RUN apt-get install -y sshpass
RUN apt-get install -y curl

RUN git clone https://gitlab.com/shockrah/site-generator /tmp/gensite
RUN cd /tmp/gensite && ./install.sh g
RUN rm -rf /tmp/gensite
```

## Gitlab's CI Script

It should be noted there are more environment variables set in Gitlab's WebUI to make this script work but basically everything is there.


```
image: shockrah/website:1.3

stages:
  - build
  - deploy

variables:
  SHOPTS: "-o StrictHostKeyChecking=no"


before_script:
  - eval $(ssh-agent -s)
  - echo "${SSH_PRIVATE_KEY}" | ssh-add - > /dev/null
  - mkdir -p ~/.ssh/
  - chmod 700 ~/.ssh/

build-site:
  stage: build

  script:
    bash ./build.sh

  artifacts:
    expire_in: 10 mins
    paths: ["./.mirror"]

deploy-vps:
  stage: deploy

  script:
    - ssh $SHOPTS root@shockrah.xyz "rm -rf /var/www/website"
    - scp $SHOPTS -r .mirror root@shockrah.xyz:/var/www/website/

deploy-neocities:
  stage: deploy

  script:
    # First the html
    - bash upload.sh -h
    # then we do the media files
    - bash upload.sh -M

```


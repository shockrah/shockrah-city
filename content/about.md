---
title: About Me
description: Yes it's me Shockrah
---

## Who I am 

I'm Shockrah, a lad on the internet that likes to make stuff. I make candy, code,
and sometimes other random crafts. This site is where I post just some of the
highlights from what I'm working on.

## About the site

Typically I just post about things that I'm working on or stuff I think might be
useful for myself or others.



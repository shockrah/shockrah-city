# Building overhaul

Apart from the switch to a more _night friendly_ site style-sheet I've also now switch to a new build system for my not-a-blog.
Full script at the bottom. [Repo link](https://gitlab.com/shockrah/site-generator).

The nice thing about this new script? Way more features, more _unixy_ so it ends up being really straight forward to use, oh and it has a nice name now **gensite**. The only major dependency it has is `pandoc` but I'm slowly going to phase that out for another script which accomplishes the same thing so far; it just can't do tables yet.

Anyway I've been messing about with more shell scripting things as I've been _ded_ for nearly 3 weeks on account of a really epic eye infection which has left me needing glasses.


```
#!/bin/bash
# Uses pandoc to turn markdown into html for my personal blog

mirror=.mirror/

assert_arg() {
	[ -z $1 ] && echo No argument provided && exit 1
}

mirror_scaffold() {
	# Setup mirror directory so that we can later truck through building new posts and pages
	mkdir -p $mirror/
	cp -r media/ $mirror/media/
	cp partials/style.css $mirror/
}

fix_tables_imgs_pure() {
	sed -i "s/{TITLE}/<title>$(basename --suffix=.html 1)<\/title>/g" "$1"
	sed -i 's/<img/<img class="pure-img"/g' "$1"
	sed -i 's/<table>/<table class="pure-table">/g' "$1"
}

create_post() {
	fname=$(basename --suffix=.md $1).html
	mkdir -p $mirror/post/
	pandoc $1 | cat partials/header.html - partials/footer.html \
		> $mirror/post/$fname

	fix_tables_imgs_pure $mirror/post/$fname
}

build_root_page() {
	fname=$(basename --suffix=.md $1).html
	mkdir -p $mirror/
	if [ ${1: -5} = ".html" ];then
		cat partials/header.html $1 partials/footer.html > $mirror/$fname
	else
		pandoc $1 | cat partials/header.html - partials/footer.html \
			> $mirror/$fname
	fi
	fix_tables_imgs_pure $mirror/$fname
}

refresh_root_src() {
	cp root/* $mirror
}

copy_media() {
	mkdir -p $mirror/media/
	cp root/style.css $mirror/
	cp -r media/ $mirror/
}

new_project() {
	mkdir -p "$1"
	cd "$1"
	mkdir -p media/fonts/ media/img/
	mkdir partials/ posts/ pages/ root/
	touch partials/header.html partials/footer.html
	echo Project \""$1"\" created.
}

_help_prompt() {
cat <<EOF
Options:
   new project_path: Create a new project folder
   P|page file.path: Build the root level page
   p|post file.path: Build that one post into the site mirror folder's posts directory
   r|root file.path: Build that root page, these live like /index.html /about.html etc.
   R|roots: Copy over the new/updated root resources
   m|media: Copy over the media folder contents
   s|scaffold: Build mirror directory
   server: Run python live server 
EOF
}

[ -z $1 ] && _help_prompt && exit 0

opt=`echo $1 | tr -d '-'`
case $opt in
	new) assert_arg $2;new_project $2;;
	P|page) assert_arg $2; build_root_page $2;;
	p|post) assert_arg $2;create_post $2;;
	r|root) assert_arg $2;build_root_page $2;;
	R|roots) refresh_root_src;;
	m|media) copy_media;;
	s|scaffold) mirror_scaffold;;
	server) cd $mirror && python -m SimpleHTTPServer 8080;;
	*) _help_prompt;;
esac
```

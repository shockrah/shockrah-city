---
title: Send me coin
description: But only if you want to
type: contact
layout: stream
links:
    - target:
        name: Ko-Fi
        url: https://ko-fi.com/shockrah
        image: https://uploads-ssl.webflow.com/5c14e387dab576fe667689cf/5cbec633ae2b882fff068659_ko-fi_horizontal-p-500.png
        description: I make wares even if you don't have coin
        title: Send me coin here if you'd like
        color: "#29ABE0"
    - target:
        name: Venmo
        url: https://venmo.com/u/alejandro-foss
        image: https://cdn1.venmo.com/marketing/images/branding/venmo-icon.svg
        description: For fellow Venmo chads | Look for the smiley
        title: "@alejandro-foss"
        color: "#3d95ce"
---



---
title: DIY Detergent
date: 2023-07-05
draft: false
description: Cheap detergent at home
category: article
image:
---

# Making detergent at home.

Recipe and instructions are followed by some interesting things I found
regarding cost and availability.

# Recipe

* 1 Part Borax
* 1 Part Baking Soda ( Sodium *Bi-Carbonate* )
* 1 Washing soda Semi-Optional ( Sodium *Carbonate* )
    * **NOTE**: Can be made from baking soda if you can't find it in stores
    * **NOTE**: While not required does greatly improve the quality of the detergent

Optional additions
* 1 Bar of Soap ( optional )
    * No liquid soap will not work for this

## Instructions

Mix Borax, Baking Soda, and Washing Soda evenly in a container. If you are using 
a bar of soap you can use a cheese grater to break it down so that you can mix
this in with the rest.

## Making Washing Soda ( Sodium *Carbonate* )

Since this stuff can be hard to find you can also make it at home.

Here's a video from __[NileRed](https://www.youtube.com/watch?v=cpGEc-pLXN4).__

Quick transcription:

1. Pour out baking soda ( sodium bi-carbonate ) into a pot
2. As it heats up it will release CO2 and water
    * This results in some bubbling
3. Keep heating until bubbling stops and the powder appears like a dense powder.


# FAQ & Stuff about cost

> Is this actually cheaper than just buying a regular detergent?

From my _very_ quick checks of prices around me I found Target had 16oz boxes of baking soda for 1$. A 4 lb box of Borax was 8$.

For 3 pounds of detergent you would be looking at a 10$ investment of materials.

* 1 lb baking soda ( 1$ )
* 1 lb baking soda converted to washing soda ( 1$ )
* 1 lb borax ( 2$ )

In total **4$** for 3 pounds of unscented detergent isn't too bad.

> Are there any alternatives to this sort of thing?

Dish soap unironically works well enough for small loads you just have
to play with the quantities.

> What about unit prices for all of these things?

Borax: https://www.intratec.us/chemical-markets/borax-price

Baking Soda: https://www.intratec.us/chemical-markets/sodium-bicarbonate-price

_Not including soap because this depends highly on what soap you choose_






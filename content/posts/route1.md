---
title: "A Quick Bhop Route Showcase"
date: 2019-10-01
draft: false
category: article
description: Routing Bhop maps can be fun sometimes
---

# Neat Route

_Short post that I've been wanting to make about a run I'm working on_

<iframe src="https://player.twitch.tv/?autoplay=false&video=v470117605" frameborder="0" allowfullscreen="true" scrolling="no" height="378" width="620"></iframe>

Some optimizations in this route are the entrance through the teleporter at the start.
Instead of falling to the left and then hitting the first boot I take a small speed loss to _always_ hit the booster.

Potentially I could save some air time if after the red platform I hit the second booster which gives less height to the thirds booster in that section, but the difference is minimal and I'm too lazy to make the flick consistently.

As soon as I fix my windows drive I'll try to get a fully timed segment of the run or something to get a better idea of time but so far I think the overall time comes out to 4:40.
_Almost 30 seconds faster than the previous world record!_

There's still quite a few places I need to improve on however so the route isn't done yet + there are some skips I want to test out.

Oh well back to loonix things for a bit :^)

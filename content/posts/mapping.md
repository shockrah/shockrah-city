---
title: Mapping in Reflex
date: 2018-03-13
draft: false
category: article
description: Looking at what makes me tick as a Reflex-Race mapper
---
# Mapping Again

[Link to the map I discuss here](https://steamcommunity.com/sharedfiles/filedetails/?id=1329660201)

## FeverStrafe

To clear up some confusion, _feverStrafe_ is a Reflex Race map I made a few months ago but never got around to finishing due to some issues with time, and motivation. After a few months I came back to see what I had made so far and realized if this was to ever be release worthy it would have to go through a through cleaning.

## Cleaning Up

In terms of what needed to be re-done, it seemed that spacing was the main issue so get past. I realized this since most of the map’s spacing seemed to be heavily derived from what CS:Source style spacing which would work there but Reflex is weird in that air control is nowhere near as flexible as in CS:Source. Of course there had to be some kind of method to make the map fun to play and satisfying to speed-run; after deliberation a solution was found.

### Modifying spacing with casual runs in mind to give competitive runners more room

The idea here is simple: speed-runners will break the map so instead of worrying about what they might think of it I focus more on casual players, making sure that the map is fun. Simply put, if the map isn’t fun to play normally, there is no chance that it will be fun to run. To make sure that the gameplay was fun I turned to the one type of race map I personally despise, strafe maps.

> strafe map: mostly flat race map with little variations in gameplay 

A good strafe map is hard to come by, which isn’t because they are hard to make but rather that it must be very clever in its use mechanics to actually be interesting. One of the additional problems strafe maps face is the inherent design of its gameplay, being flat this means that a mapper typically has to come up with some theme or a gameplay gimmick to make it memorable. The good thing about them is that the whole map is available to nearly any kind of player since they are also very easy maps to complete as a player. Making more of _feverStrafe_ available to everyone is exactly what the map needed to feel more complete and flow more easily. The map already had multiple levels, some meant for speed, some technically challenging, others were a mix of both. This meant that instead of changing what gameplay I already had, I simply added easy and hard routes. I kept in mind what the world record runs might look like, and essentially filled in the gaps that most players would probably fall into. To me these maps suffer from nearly the same issue auto-hop maps had (and still do sometimes) in CS:Source. To quote Badges:

> The map itself pretty much dictates the route and doesn’t allow the player much room to find faster ways around the map, resulting in banal, braindead gameplay. Maps like this aren’t very competitive and result in most of the top times being very similar. bhop_muchfast is a good example of this. Creating maps with more route options by adding more bhop blocks with less space between them will make your map more popular and competitive by allowing newer players to experience them, seasoned players to speedrun them, and normal / scroll players to play them. The top times will have much more variety and every run of the map will be different. bhop_cw_journey, for example.

[Full quote/post here](https://gamebanana.com/maps/182909)

For _feverStrafe_ this meant trying to get a balance where the map would be fun to play in general for everyone while providing memorable gameplay. I think after looking at what _feverStrafe_ turned into I can fully say that I am actually somewhat proud of what it is now.

------

## After thoughts and future maps

As a final say to Reflex, I’m only leaving because the game has favored competitive duel for so long that every other gamemode apart from race is nearly dead, even for Reflex’s standards. Because I don't actually play or enjoy duel anymore it means there’s really nothing left to do besides make maps which I can do for any game, so why not one where there’s more going on. Maybe it will get better but it doesn’t seem to be going anywhere, looking at Reflex as a passion project however, it’s doing great and cant’ wait to see what the guys at Pixel come up with.

_p.s. dubs4lyfe_








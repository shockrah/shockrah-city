import hashlib

with open('public/tags/index.xml') as file:
    data = hashlib.sha1(file.read().encode('utf-8'))

remote = '99d66a9e171feaf11be88b831bc69c55d85c1b4b'
print(remote == data.hexdigest())

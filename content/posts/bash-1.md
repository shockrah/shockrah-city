---
title: "Bash is weird sometimes"
date: 2018-07-22
draft: false
description: Strangeness in the autocomplete feature
category: article
---

# Bash is weird sometimes

Bash is actually pretty nice but is... weird sometimes. \
Take this for instance: `alias v='vim'`
This is an actual bind I use daily however if you don't have tab-completion setup for vim you know that tab-completion usually results in:

> vim myFi\<tab\> \
> cd: too many arguments\
> bash: cd: too many arguments

As fun as that is to deal with(_it's not_) apparantly that bind I mentioned earlier fixes this issue completely. \
I'm not really sure why that works, but I'm just glad it does. 
If you've ever needed a way of allowing for tab-completion with vim now you have one. \
Just use that alias in your bashrc and you're good to go.


## Rambled guess time

My guess on how the alias is that bash doesn't try to figure out that you want to use `vim` since you've only typed a single `v` in this case; therefore tab-completion would go unnoticed?\
Say you had a directory with files:

> first \
> second \
> third 

That would mean if your shell contained `asdf`. You could go to the first character start typing any one of those file names and have bash guess the filename to autocomplete, since it only look backwards for completion.

### Where be the posts

Lately I've been working on a translator for html. Similar to how pandoc turns markdown into valid html which lead me onto another issue that I've also taken on since I've not much to do. \
Specifically it's looking into _GCC_'s seemingly random behavior with switch cases. 
Essentially _GCC_ will sometimes create a decision tree for a slightly faster search time through the cases, sometimes create the equivalent of linearly executable code(_like a bunch of if/else's_), and if given the opportunity "_guess_" the correct case on the first check... 
For that reason I've started diving into it to try and determine what the fug is going with this compiler and what flags, if any can be used to create some more predictable output.
Hopefully the research crops up something interesting to think but who knows 🤷.

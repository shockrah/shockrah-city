---
title: "Making Ice Cream at Home"
date: 2020-07-24
draft: false
category: article
description: A simple recipe that works for basically any flavor
---

# Making Ice Cream at Home

**NOTE**: While I don't use an ice cream maker, I do suggest a hand mixer but this can be done with as little as a fork/spoon to mix things(doing that sucks though).


So here's what I used to make the pic below:
{{< pic "/media/img/baking/blackberry-icecream.jpg" >}}


## Recipe Base

* 2 Cups / 475 mL -> Heavy Cream

* 1 Cup / 237 mL -> Evaporated Milk

* 3/4 Cup / 177 mL -> Sugar

* 1 Tablespoon Vanilla Extract (I used vanilla flavoring)

* Pinch of salt

## Toppings/Chunks/Flavorings 

* 6 oz Blackberry

Note that this is added to taste and 6 oz is only what I had on hand at the time of making.


## Instructions

### Prep Steps

1. Prep the bowls/ingredients

Take a medium sized bowl and place into a much larger bowl.
Before you open them, toss the milk/cream into the bowls to chill for about 15 minutes in the freezer.

2. Ice the larger bowl

Put ice + salt into the larger bowl and mix. This lowers the freezing point of the ice so it melts and gets supercooled in the freezer;chilling the ice cream more evenly later on.
Put enough ice to line the smaller bowl.

### Base Steps

3. Basically dump everything into the small bowl and mix until the mixture is peaking.

It's gonna seem like your making a whipped cream at this point, but that's because we're dumping as much air as we can upfront instead of later.

4. Toss in the freezer until the top has the consistency of ice cream.

### Final mixings/Toppings

5. For any fruit chunks seperate the fluid out to pour into the base mixture.

I do this for color/flavoring, but mostly color.

6. Finally mix in the chunks and seal in a sealable container in the freezer.




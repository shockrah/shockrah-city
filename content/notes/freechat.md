---
title: Freechat Current Thoughts
date: 2021-12-13
draft: true
description: A mid-mortem thing about the project's status
category: article
image: "/media/"
---

# What is Freechat

Freechat is my attempt at creating a chatting platform with developers in mind.
It's built with the idea that a chat interface shouldn't be difficult to
understand and thus adopts an API that (in my eyes) makes sense.

# Project's Status

The old code base was pretty close to unmaintainable after a certain point
so I'm basically throwing it all out in favor of building on a framework
that doesn't blow.
Where before I was only only Hyper.rs, I'm moving the server logic to Rocket.rs.
For now the roadmap looks something like this:

* Rebuild endpoints 


# Why build this at all?

I'm 100% sick of using Discord, not just as a user but as a maintainer of a
few communities and bots. There is a huge reliance on Discord for communicating
and it's getting to the point where if it failed for some reason there would be
basically no where to go to.






